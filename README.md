# ansible2k8s

How to create a K8s Kluster in 30min 

# Prereq
For now works on 3 Ubuntu bionic LTS servers (18.04) with at least 2 CPU 2Go ram

Have ansible installed on a 3rd one

# Instruction 
Supposing you already have 3 servers (desc above) that you can connect to with a public ip through ssh 
*  Connect at least once to each of the 3 servers you dispose 
```sh
ssh PUBLIC_IP
```
*  Replace your values in inventory.ini
*  From this directory run
```sh
ansible-playbook -vvv -i inventory.ini playbook/master-playbook.yml
```
*  then
```sh
ansible-playbook -vvv -i inventory.ini playbook/nodes-playbook.yml
```
* thats it

### Credits
All credits goes to [graspin tech](https://graspingtech.com/create-kubernetes-cluster/)
All i did was adaptate from vagrant to a real Cloud provider like GCP AWS IBM...